package hledger

import (
	"golang.org/x/text/currency"
	"log/slog"
)

type Tag struct {
	Key   string
	Value string
}

type Account struct {
	Path string
	Tags []*Tag
}

type Transfer struct {
	Account  *Account
	Value    currency.Amount
	Currency currency.Unit
}

type Transaction struct {
	Date        string
	Description string
	Comment     string
	Tags        []*Tag
	Transfers   []*Transfer
}

type Journal struct {
	Accounts     []*Account
	Transactions []*Transaction
}

func Write(filename string, journal Journal) (err error) {
	slog.Warn("STUB: hledger.Write(filename string, journal Journal)", "accounts", len(journal.Accounts), "transactions", len(journal.Transactions))
	return nil
}
